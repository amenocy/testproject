package amin.abb.testproject;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private EditText urlInput;
    private Button dlButton;
    private long downloadId;
    private ImageView image;
    /*
    create receiver and check for action
     */
    public BroadcastReceiver downloadCompleteReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                openImage(context, id);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        /*
        check if network connection is not availble
         */
        if (!checkNetwork()) {
            showNoNetworkError();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
        listen to tap on download button
         */
        dlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check for input string should valid url
                if (!isUrl(urlInput.getText().toString())) {
                    showURLError();
                } else {
                    // we need permission to save file on disk, so we should ask it.
                    if(checkDownloadPermission()) {
                        Uri image_url = Uri.parse(urlInput.getText().toString());
                        downloadImage(image_url);
                    }
                }
            }
        });
    }

    private void downloadImage(Uri image_url) {
        // register a BroadcastReceiver in DownloadManager to receive completed downloads
        registerReceiver(downloadCompleteReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        DownloadManager dlManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request req = new DownloadManager.Request(image_url);
        req.setTitle("Downloading image");
        req.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, image_url.getLastPathSegment());
        //save download id to compare it later
        downloadId = dlManager.enqueue(req);
    }

    private void openImage(Context context, long id) {
        // check for download id is equals to our current downloadId
        if (id == downloadId) {
            // get full downloaded file data from download manager
            DownloadManager dlManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Query q = new DownloadManager.Query();
            q.setFilterById(downloadId);
            Cursor cursor = dlManager.query(q);
            if (cursor.moveToFirst()) {
                int downloadStatus = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                String localUriString = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                if ((downloadStatus == DownloadManager.STATUS_SUCCESSFUL) && localUriString != null) {
                    Uri localUri = Uri.parse(localUriString);
                    // create a bitmap from downloaded file , we need to Rotate image from bitmap
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(localUri.getPath(), options);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(180);
                    Bitmap new180bit = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                            matrix, true);

                    image.setImageBitmap(new180bit);
                }
            }
            cursor.close();
        }
    }

    private void showURLError() {
        //shake input edittext to show error
        Animation errorAnim = AnimationUtils.loadAnimation(this, R.anim.errorshake);
        urlInput.startAnimation(errorAnim);
    }

    private void initViews() {
        urlInput = (EditText) findViewById(R.id.url);
        dlButton = (Button) findViewById(R.id.download);
        image = (ImageView) findViewById(R.id.image);
    }


    private boolean isUrl(String url) {
        Pattern pattern = Patterns.WEB_URL;
        return pattern.matcher(url.toLowerCase()).matches();
    }

    private boolean checkNetwork() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private void showNoNetworkError() {
        Snackbar.make(urlInput, "Check network connection", Snackbar.LENGTH_INDEFINITE).show();
    }

    // request permission from user
    public boolean checkDownloadPermission() {
        int api = Build.VERSION.SDK_INT;
        if (api >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    android.support.v7.app.AlertDialog.Builder alertb = new android.support.v7.app.AlertDialog.Builder(this);
                    alertb.setCancelable(true);
                    alertb.setTitle("permission");
                    alertb.setMessage("please let us download image to your donwloads folder");
                    alertb.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        }
                    });
                    android.support.v7.app.AlertDialog alert = alertb.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    // listen to permission request's result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dlButton.performClick();
                } else {
                    Toast.makeText(MainActivity.this,"Download file blocked",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
